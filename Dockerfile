FROM lolhens/baseimage-openjre
EXPOSE 8080
ADD target/demo12.jar demo12.jar
ENTRYPOINT ["java","-jar","demo12.jar"]
package com.example.demo12;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Demo12Application {

	@GetMapping("/message")
	public String getMessage() {
		return "congrats its working";
	}
	public static void main(String[] args) {
		SpringApplication.run(Demo12Application.class, args);
	}

}
